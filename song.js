// Define song details using var
var trackTitle = "Good Night Good Bye"
var artist = "Tjahjo Wisanggeni"
var album = "From The Other Side"
var released = "1992"
var songLengthInSeconds = 360
var isUnderRated = true
var songGenre = ["Instrumental Rock", "	Neoclassical Metal"]
var songWriters = "Tjahjo Wisanggeni"

// Output each var(s) using console.log
console.log( "Track title: " + trackTitle, 
"Artist: " + artist, 
"Album: " + album, 
"Released: " + released, 
"Song duration: " + songLengthInSeconds + " seconds", 
"Under rated: " + isUnderRated, 
"Song Genre: " + songGenre, 
"Song writer(s):" + songWriters)